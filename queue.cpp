#include "queue.h"
#include <iostream>

void initQueue(queue* q, unsigned int size)
{
	q->element = new int[size];
	q->size = size;
	q->place = 0;
	newQueue(q);
}

void newQueue(queue* q)
{
	for (int i = 0; i < q->size; i++)
	{
		q->element[i] = -1;
	}
}

void cleanQueue(queue* q)
{
	delete[] q->element;
	
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->size == q->place)
	{
		std::cout << "There is no place left in the queue to enter a value" << std::endl;
	}
	else
	{
		q->element[q->place] = newValue;
		q->place++;
	}
}

int dequeue(queue* q)
{
	int val = q->element[0];
	int temp_val = 0;
	int i = 0;
	for (i = 0; i < q->place - 1; i++)
	{
		temp_val = q->element[i + 1];
		q->element[i] = temp_val;
	}
	q->element[i] = -1;
	q->place--;
	return val;
}

void print(queue* q)
{
	for (int i = 0; i < q->size; i++)
	{
		std::cout << "q->element[" << i << "] = " << q->element[i] << std::endl;
	}
	std::cout << std::endl;
}
