#include "stack.h"
#include "linkedList.h"
#include <iostream>

void push(stack* s, unsigned int element)
{
	if (s->first == NULL)
	{
		s->first = new linkedList;
		s->first->value = element;
		s->first->next = NULL;
	}
	else
	{
		s->first = add(s->first, element);
	}
}
int pop(stack* s)
{
	int val = -1;
	if (s->first != NULL)
	{
		val = s->first->value;
		s->first = s->first->next;
	}
	return val;
}

void initStack(stack* s)
{
	s->first = NULL;
}

void cleanStack(stack* s)
{
	linkedList** temp1 = &s->first;
	linkedList* temp2 = NULL;
	while ((*temp1)->next != NULL)
	{
		temp2 = (*temp1)->next;
		delete (*temp1);
		*temp1 = temp2;
	}
}

void print(stack* s)
{
	linkedList* temp = s->first;
	while (temp != NULL)
	{
		std::cout << "q->linedlist = " << temp->value << std::endl;
		temp = temp->next;
	}
	
	std::cout << "q->linedlist = null" << std::endl;
	std::cout << std::endl;
}
